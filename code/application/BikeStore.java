package application;
import vehicles.Bicycle;

public class BikeStore {
    
    public static void main(String[] args) {

        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Rida", 9, 150);
        bikes[1] = new Bicycle("Michelle", 1, 0.05);
        bikes[2] = new Bicycle("Teacher", 20, 300);
        bikes[3] = new Bicycle("nike", 6, 70);

        for(int i = 0; i < bikes.length; i++) {
            System.out.println(bikes[i]);
        }


    }
}
