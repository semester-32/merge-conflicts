// Rida Chaarani
// 2137814
package vehicles;

public class Bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    
    public Bicycle(String manuf, int numGears, double speed) {
        this.manufacturer = manuf;
        this.numberGears = numGears;
        this.maxSpeed = speed;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public double maxSpeed() {
        return this.maxSpeed;
    }

    public String toString() {
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }


}